#!/usr/bin/env node

import path from "path"

import { fileURLToPath } from 'url';

const __dirname = path.dirname(fileURLToPath(import.meta.url));

import webpack from 'webpack'

import { createRequire } from 'module';
const require = createRequire(import.meta.url);

export default class MJSCompiler {

  constructor(wctx_path, config) {

    this.wctx_path = wctx_path;
    this.config = config;

    let webpack_plugins = [
      new webpack.ProvidePlugin({
        Buffer: ['buffer', 'Buffer'],
        process: ['process'],
      })
    ];

    let webpack_optimization = {};


    const babel_env = {
      "useBuiltIns": "usage",
      "corejs": { version: "3.8", proposals: true }
    }

    const pack_entry = {};
    if (!config.legacy) {
      pack_entry[`./${config.dst}`] = [path.resolve(this.wctx_path, config.src)];
      babel_env.targets = {
        browsers: [
          'Chrome >= 60',
          'Safari >= 10.1',
          'iOS >= 10.3',
          'Firefox >= 54',
          'Edge >= 15',
        ]
      };
    } else {
      pack_entry[`./${config.dst}.es5`] = [path.resolve(this.wctx_path, config.src)];
    }

    this.compiler = webpack({
  /*      watch: true,
        watchOptions: {
          aggregateTimeout: 300,
          poll: 1000
        },*/
        mode: 'development',
        entry: pack_entry,
        output: {
          path: this.wctx_path,
          filename: '[name].js'
        },
        cache: false,
        plugins: webpack_plugins,
        optimization: webpack_optimization,
        resolve: {
          modules: [
            "...",
            path.resolve(__dirname, "node_modules")
          ],
          fallback: {
     //       assert: require.resolve("assert"),
            buffer: require.resolve("buffer/"),
//            console: require.resolve("console-browserify"),
//            constants: require.resolve("constants-browserify"),
            crypto: require.resolve("crypto-browserify/"),
//            domain: require.resolve("domain-browser"),
            events: require.resolve("events/"),
            http: false,
//            http: require.resolve("stream-http"),
//            https: require.resolve("https-browserify"),
//            os: require.resolve("os-browserify/browser"),
            path: require.resolve("path-browserify/"),
            punycode: require.resolve("punycode/"),
            process: require.resolve("process/browser"),
//            querystring: require.resolve("querystring-es3"),
            stream: require.resolve("stream-browserify/"),
//            _stream_duplex: require.resolve("readable-stream/duplex"),
//            _stream_passthrough: require.resolve("readable-stream/passthrough"),
            _stream_readable: require.resolve("readable-stream/readable"),
//            _stream_transform: require.resolve("readable-stream/transform"),
//            _stream_writable: require.resolve("readable-stream/writable"),
            string_decoder: require.resolve("string_decoder/"),
            sys: require.resolve("util/"),
//            timers: require.resolve("timers-browserify"),
//            tty: require.resolve("tty-browserify"),
            url: require.resolve("url"),
            util: require.resolve("util/"),
//            vm: require.resolve("vm-browserify"),
            zlib: require.resolve("browserify-zlib"),
            module: false
          }
        },
        module: {
            rules: [
              {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                  loader: path.resolve(__dirname, "node_modules", 'babel-loader'),
                  options: {
                    presets: [
                      [
                        path.resolve(__dirname, "node_modules", '@babel/preset-env'),
                        babel_env
                      ]
                    ],
                    plugins: [
                      path.resolve(__dirname, "node_modules", '@babel/plugin-proposal-class-properties')
                    ]
                  }
                }
              },
              { test: /\.css$/, use: 'css-loader' },
              {
                test: /\.less$/i,
                use: [
                  // compiles Less to CSS
                  "style-loader",
                  "css-loader",
                  "less-loader",
                ],
              },
              {
                test: /\.(html|njk)$/,
                use: {
                  loader: 'html-loader',
                  options: {
                    esModule: false
                  }
                }
              },
              {
                test: /\.(vs|fs)$/,
                use: {
                  loader: 'raw-loader'
                }
              },
              {
                test: /^http$/,
                use: {
                  loader: 'null-loader'
                }
              }
            ]
        },
        stats: {
          colors: true
        },
        devtool: 'source-map'
    });

  }


  async compile(next) {
    try {
      let _this = this;

      await new Promise((resolve) => {
        this.compiler.run((err, stats) => {
          if (err) console.error(err);
          _this.webpack_report(stats);
          if (typeof next === "function") next(stats);
          _this.compiler.close((closeErr) => {
            if (closeErr) console.error(closeErr);
            resolve();
          });
        });
      });

    } catch (e) {
      console.error(e.stack);
    }
  }

  webpack_report(stats) {
    let report_time = new Date();
    report_time = report_time.toDateString() + " " + report_time.toTimeString().slice(0, report_time.toTimeString().indexOf("GMT")-1);

    if (stats.hasErrors()) {
      let info = stats.toJson();
      let errors = info.errors;

      for (let e = 0; e < errors.length; e++) {
        let err_ostr = "\n\x1b[34m"+report_time+"\n"+"\x1b[31mERROR IN: \x1b[33m"+errors[e].moduleName+"\x1b[0m\n";
        err_ostr += errors[e].stack;

        console.log(err_ostr);
      }
    } else {
      console.log("\x1b[34m"+report_time, "\x1b[1m\x1b[36mWEBPACK\x1b[0m -->", "\x1b[36m"+this.wctx_path+`/${this.config.dst}.js\x1b[0m`);
      if (stats.hasWarnings()) {
        let info = stats.toJson();
        let errors = info.warnings;
        
        for (let e = 0; e < errors.length; e++) {
          if (errors[e].details) {
            let err_lines = errors[e].details.split("\n");
            err_lines[0] = "\x1b[33mWARNING: "+err_lines[0]+"\x1b[0m";

            let err_ostr = "";
            for (let l = 0; l < err_lines.length; l++) {
              err_ostr += err_lines[l]+"\n";
            }
            console.log(err_ostr);
          }
        }
      }
    }
  }
}

const CWD = process.cwd();

(async () => {
  try {
    const mjsc = new MJSCompiler(
      CWD,
      {
        src: "src/index.mjs",
        dst: "main"
      }
    );

    mjsc.compile();

  } catch (e) {
    console.error(e.stack);
  }
})();
