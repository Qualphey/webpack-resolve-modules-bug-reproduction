
# Steps
1. `git clone https://gitlab.com/Qualphey/webpack-resolve-modules-bug-reproduction.git`
2. `cd webpack-resolve-modules-bug-reproduction/script`
3. `npm install`
4. `cd ../test`
5. `../script/index.mjs`

## Result
```
test]$ ../script/index.mjs

Thu Apr 04 2024 16:32:11
ERROR IN: undefined
ModuleNotFoundError: Module not found: Error: Can't resolve 'babel-loader' in '/home/qualphey/Development/Tests/flytests/flypack/webpack-resolve-modules-bug-reproduction/test'
    at /home/qualphey/Development/Tests/flytests/flypack/webpack-resolve-modules-bug-reproduction/script/node_modules/webpack/lib/Compilation.js:2094:28
    at /home/qualphey/Development/Tests/flytests/flypack/webpack-resolve-modules-bug-reproduction/script/node_modules/webpack/lib/NormalModuleFactory.js:895:13
    at eval (eval at create (/home/qualphey/Development/Tests/flytests/flypack/webpack-resolve-modules-bug-reproduction/script/node_modules/tapable/lib/HookCodeFactory.js:33:10), <anonymous>:10:1)
    at /home/qualphey/Development/Tests/flytests/flypack/webpack-resolve-modules-bug-reproduction/script/node_modules/webpack/lib/NormalModuleFactory.js:332:22
    at eval (eval at create (/home/qualphey/Development/Tests/flytests/flypack/webpack-resolve-modules-bug-reproduction/script/node_modules/tapable/lib/HookCodeFactory.js:33:10), <anonymous>:9:1)
    at /home/qualphey/Development/Tests/flytests/flypack/webpack-resolve-modules-bug-reproduction/script/node_modules/webpack/lib/NormalModuleFactory.js:630:15
    at /home/qualphey/Development/Tests/flytests/flypack/webpack-resolve-modules-bug-reproduction/script/node_modules/webpack/lib/NormalModuleFactory.js:151:11
    at /home/qualphey/Development/Tests/flytests/flypack/webpack-resolve-modules-bug-reproduction/script/node_modules/webpack/lib/NormalModuleFactory.js:706:8
    at /home/qualphey/Development/Tests/flytests/flypack/webpack-resolve-modules-bug-reproduction/script/node_modules/neo-async/async.js:2830:7
    at done (/home/qualphey/Development/Tests/flytests/flypack/webpack-resolve-modules-bug-reproduction/script/node_modules/neo-async/async.js:2925:13)
```

# Validation
There is also the same `src` directory in `/script/` as in `/test/` so that you can see it does bundle whith `node_modules` in cwd.
1. `cd ../script/`
2. `./index.mjs`

## Result
```
script]$ ./index.mjs
Thu Apr 04 2024 16:32:02 WEBPACK --> /home/qualphey/Development/Tests/flytests/flypack/resolve-modules-bug-reproduction/script/main.js
```
